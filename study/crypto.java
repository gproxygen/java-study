package crypto;

import java.util.Scanner;
import java.util.Arrays;

// ������ ������� ������� ������ inData ������� ������������, �������� � key
public class crypto {
	
	static String 	inData, outData="";
	static int offset=0;			// �������� ������ ������ (�� ��������� ����� �����)
	static int[] key= {3,0,2,1};	// ������� ������������, ��������� � ����!
	
	static void Console(String v) {  // ����� ������ � �������
		System.out.println(v);
	}
	
	static boolean notEnd() {		// ���� true ����� ���� ���� �� offset		
		return inData.length()>=offset + key.length;		
	}
	
	static void Next() {	// ������� ������� ����
		for(int j=0;j<key.length;j++)
			outData+=inData.charAt(offset+key[j]); 
		offset+=key.length;
	}

	static void Append(char v) {  // ��������� ��������� ���� (����� �� ��������� ����� ������)
		while(inData.length()%key.length>0)
			inData+=v;		
	}

	public static void main(String[] args) { // ������� ���������� �������
		System.out.println("��������������� ���� " + Arrays.toString(key) + ". ������� ��� ������:");
		Scanner scan = new Scanner(System.in);		
		inData = scan.nextLine();			
		Append(' ');
		while(notEnd()) {
			Next();	
		}			
		Console(outData);		
		scan.close();
	}
}