// Подключения необходимых библиотек
import javax.imageio.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.apache.commons.lang3.*;



// Класс панели, которая является игровым полем
class pole extends JPanel
{
	private Image hero; // Закрытая Переменная класса, в которую загружается шапка
	private Image fon; // Закрытая Переменная класса, в которую загружается фон
	public int x = 400; // Открытая Переменная класса, в которую загружается шапка
	private int slogn; // Переменная сложности игры
	private podar[] gamePodar; // Массив подарков
	private life[] gameLife; // Массив жизней
	private Image end_game; // Изображение Окончания игры
	public Timer timerUpdate,timerDraw; // Два таймера: первый для
	private int lifes; //  число жизней
	int floor;


	// Конструктор класса
	public pole(int slogn,int lifes,int delay,int loseLevel)
	{

		this.slogn = slogn;
		this.lifes = lifes;
		this.floor = loseLevel;
		// Загрузка изображения шапки из файла
		try
		{
			hero = ImageIO.read(new File("./img/hero.png"));
		}
		catch(IOException ex) {}

		// Загрузка изображения фона из файла
		try
		{
			fon = ImageIO.read(new File("./img/background.png"));
		}
		catch(IOException ex) {}

		// Загрузка изображения Окончания игры
		try
		{
			end_game = ImageIO.read(new File("./img/gameover.png"));
		}
		catch(IOException ex) {}

		//  Загрузка  жизней
		gameLife = new life[lifes];
		for (int i=0;i<lifes;i++)
		{
			try
			{
				gameLife[i] = new life(ImageIO.read(new File("./img/life.png")),720-50*i);
			}
			catch (IOException ex) {}
		}


//		   ArrayUtils.add(gameLife, gameLife[0]);

		//  Загрузка изображений подарков
		gamePodar = new podar[slogn];
		for (int i=0;i<slogn;i++)
		{
			try
			{
				gamePodar[i] = new podar(ImageIO.read(new File("./img/p"+i+".png")),floor);
			}
			catch (IOException ex) {}
		}



		// Создание таймера, который будет проверять и добавлять подарки на игровое поле
		timerUpdate = new Timer(delay,new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateStart(); // Метод для проверки и добавление подарков на игровое поле

			}
		});
		timerUpdate.start(); // Запуск таймера timerUpdate

		// Создание таймера, который будет перерисовывать игровое поле 20 раз в секунду
		timerDraw = new Timer(50,new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				repaint(); // Запуск метода перерисовки поля (public void paintComponent(Graphics gr))
			}
		});
		timerDraw.start(); // Запуск таймера для перерисовки
	}


	void endGame(Graphics gr){
		super.paintComponent(gr);
		gr.drawImage(fon, 0, 0, null); // Рисование фона
		gr.drawImage(hero, x, 465, null); // Рисование шапки
		gr.drawImage(end_game, 300, 150, null); // Вывод картинки Окончания игры

		timerDraw.stop();
		timerUpdate.stop();

	}

	// Метод, который отрисовывает графические объекты на панели
	public void paintComponent(Graphics gr)
	{
		// Выполнить отрисовку сначала самого окна
		super.paintComponent(gr);

		gr.drawImage(fon, 0, 0, null); // Рисование фона
		gr.drawImage(hero, x, 465, null); // Рисование шапки

		// Цикл, который отображает жизни на игровом поле
		for (int i=0;i<gameLife.length;i++)
		{
			gameLife[i].draw(gr); // Отображение подарка
		}
		// Цикл, который отображает подарки на игровом поле и проверяет пропущенные подарки
		for (int i=0;i<slogn;i++)
		{
			gamePodar[i].draw(gr); // Отображение подарка
			if (gamePodar[i].act==true) // Если подарок из массива подарков активен
			{
				if ((gamePodar[i].y+gamePodar[i].img.getHeight(null))>=floor) // Если подарок достиг нижней границы
				{
					if (Math.abs(gamePodar[i].x - x) > 75) // Если подарок пропущен
					{
						gameLife = ArrayUtils.remove(gameLife, 0);
						gamePodar[i].act=false;
						boolean gameOver = gameLife.length==0;
						if (gameOver) {
							endGame(gr);
							break; // Прерывание цикла
						}
					}
					else gamePodar[i].act=false; // Снятие подарка с игрового поля, если он пойман шапкой
				}
			}
		}

	}
	// Метод для проверки и добавление подарков на игровое поле
	private void updateStart()
	{
		int kol=0; // Переменная для подсчета подарков на игровом поле
		for (int i=0;i<slogn;i++) // Цикл перебора всех подарков массива
		{
			if (gamePodar[i].act==false) // Если подарок не на игровом поле
			{
				if (kol<slogn) // Если текущее количество менее номера сложности (от 1 до 7)
				{
					gamePodar[i].start(); // Активизация подарка на игровом поле, вывод его сверху игрового поля
					break; // Прерывание цикла
				}
			}
			else kol++; // Если подарок на игровом поле
		}
	}
}