
// Подключения необходимых библиотек
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

// Класс для подарка, падающего сверху
class podar {

	public Image img; // Изображение подарка
	public int x, y, dir, rate; // Положение подарка на игровом поле, в пикселях, x - отступ слева, y - отступ
	// сверху, dir - направление движения 0-в 1-н 2-л 3-п 4-пв 5-пн 6-лв 7-лн
	public Boolean act; // Переменная логического типа, показывающая активность подарка, есть он игровом
	// поле или нет
	Timer timerUpdate; // Таймер, отвечающий за движение подарка вниз
	int floor; // уровень, когда подарок считается пропущенным

	// Конструктор класса
	public podar(Image img,int floor) {
		this.floor = floor;
		// Создание и настройка таймера, отвечающего за движение подарка
		timerUpdate = new Timer(500, new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				switch (dir) {// Методы, осуществляющие движение подарка вниз
					case (0):
						move_up();
						break;
					case (1):
						move_down();
						break;
					case (2):
						move_left();
						break;
					case (3):
						move_right();
						break;
					case (4):
						move_rUp();
						break;
					case (5):
						move_rDown();
						break;
					case (6):
						move_lUp();
						break;
					default:
						move_lDown();
						break;
				}
				if ((y + img.getHeight(null)) >= floor) // Если подарок достиг нижней границы
				{
					timerUpdate.stop(); // Остановка таймера
				}

			}
		});
		this.img = img; // Передача изображения из круглых скобок Конструктора класса в переменную
		// класса
		act = false; // Изначально делаем подарок неактивным, отсутствующим на игровом поле
	}

	public void move_up() { // 0-в, отражение: 2-л 3-п 7-лн 5-пн
		if (!act)
			return;
		y -= rate;
		if (y<= 0) {
			int i = (int) (Math.random()*4);
			dir = i==0 ? /**/2/**/ : i==1 ? /**/3/**/ : i==2 ? /**/7/**/: /**/5/**/;
		}
	}
	public void move_down() {	// 1-н, отражение: НЕВОЗМОЖНО, должны поймать :)
		if (act) // Если подарок активен на игровом поле
		{
			y += rate;
		}
	}

	public void move_left() {  // 2-л, отражение: 4-пв 5-пн 0-в 1-н
		if (!act)
			return;
		x -= rate;
		if (x <= 0) {
			int i = (int) (Math.random()*4);
			dir = i==0 ? /**/4/**/ : i==1 ? /**/5/**/ : i==2 ? /**/0/**/: /**/1/**/;
		}
	}

	public void move_right() { // 3-п, отражение: 6-лв 7-лн 0-в 1-н
		if (!act)
			return;
		x += rate;
		if (x+img.getWidth(null) > 800) {
			int i = (int) (Math.random()*4);
			dir = i==0 ? /**/6/**/ : i==1 ? /**/7/**/ : i==2 ? /**/0/**/: /**/1/**/;
		}
	}

	public void move_rUp() {	// 4-пв, отражение: 1-н 2-л 5-пн
		if (!act)
			return;
		x += rate;
		y -= rate;
		if ((y <= 0) || (x+img.getWidth(null) > 800)) {
			int i = (int) (Math.random()*3);
			dir = i==0 ? /**/1/**/ : i==1 ? /**/2/**/ : /**/5/**/;
		}
	}

	public void move_rDown() {	// 5-пв, отражение: 0-в 2-л 7-лн
		if (!act)
			return;
		x += rate;
		y += rate;
		if ((y + img.getHeight(null) >= floor)  || (x+img.getWidth(null) > 800)) {
			int i = (int) (Math.random()*3);
			dir = i==0 ? /**/0/**/ : i==1 ? /**/2/**/ : /**/7/**/;
		}
	}
	public void move_lUp() {	// 6-лв, отражение: 1-н 3-п 4-пв
		if (!act)
			return;
		x -= rate;
		y -= rate;
		if ((y<0)  || (x<0)) {
			int i = (int) (Math.random()*3);
			dir = i==0 ? /**/1/**/ : i==1 ? /**/3/**/ : /**/4/**/;
		}
	}
	public void move_lDown() {	// 7-пв, отражение: 0-в 3-п 5-лн
		if (!act)
			return;
		x -= rate;
		y += rate;
		if ((y + img.getHeight(null) >= floor)  || (x<0)) {
			int i = (int) (Math.random()*3);
			dir = i==0 ? /**/0/**/ : i==1 ? /**/3/**/ : /**/5/**/;
		}
	}

	// Метод, выполняющий активизацию подарка на игровом поле
	public void start() {
		timerUpdate.setDelay(100); // Установка временной задержки для таймера
		timerUpdate.start(); // Запуск таймера
		y = 600 / 2 - img.getHeight(null)/2; // Отступ сверху в пикселях (из сообращений что по вертикали 600 точек)
		x = 800 / 2 - img.getWidth(null)/2;// (int)(Math.random()*700); // Отступ слева в пикселях, получаем случайным
		// образом от 0 до 700
		act = true; //
		// скорость движения - смещение в пикселях
		rate = 6;
		// траектория движения 0-в 1-н 2-л 3-п 4-пв 5-пн 6-лв 7-лн
		dir = (int) (Math.random()*8);
	}



	// Метод, выполянющий отрисовку подарка на игровом поле, если он активен
	public void draw(Graphics gr) {
		if (act == true) {
			gr.drawImage(img, x, y, null); // Рисование изображения
		}
	}
}
