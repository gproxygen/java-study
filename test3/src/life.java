
import java.awt.*;

// Класс для подсветки оставшихся жизней
class life {

    public Image img; // Изображение жизни
    public int x,y; // Положение на игровом поле, в пикселях, x - отступ слева, y - отступ сверху

    // Конструктор класса
    public life(Image img, int offset)
    {
        this.img = img; // Передача изображения из круглых скобок Конструктора класса в переменную класса
        this.x = offset;
        this.y = 3;
    }

    // Метод, выполянющий отрисовку на игровом поле, если он активен
    public void draw(Graphics gr)
    {

        gr.drawImage(img,x,y,null); // Рисование изображения
    }
}
