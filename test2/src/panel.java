import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

class panel extends JPanel {

    int delta, xCenter = 280, yCenter = 280;    // шаг смещения стрелки, центр циферблата
    int angle_fast,angle_slow;  // для смещения стрелок (в градусах)
    public Timer timerUpdate,timerBody;

    // конструктор, параметры - смещение (угол), задержка таймера, смещение хода (6 град. соотв. сек.)
    public panel(int angle, int delay, int delta) {

        angle_fast = angle;
        angle_slow = angle;
        this.delta = delta;

        timerUpdate = new Timer(delay, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        });

        timerBody = new Timer(delay, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // угол для быстрой стрелки (ПРОТИВ часовой)
                angle_fast -= delta;
                if(angle_fast<0)
                    angle_fast+=360;
                // угол для медленной стрелки (ПО часовой)
                angle_slow = angle_fast%(2*delta)==delta? angle_slow : (angle_slow+delta)%360;
                // если стрелки совпали
                if (angle_fast==angle_slow) {
                    int newDelay = timerBody.getDelay()>>1;
                    // если скорость не максимальная, то уменьшаем задержку
                    if (newDelay>5) {
                        timerUpdate.setDelay(newDelay);
                        timerBody.setDelay(newDelay);
                        System.out.println("В точке [" + angle_slow + "] град. установлена задержка = " + newDelay + " мс");
                    }
                }
            }
        });
        timerUpdate.start();
        timerBody.start();
    }

    // отрисовка линии (холст, цвет, угол, смещение (от центра), радиус
    void DrawMyLine(Graphics gr,Color clr,int deg,int offset, int radius) {
        double angle = deg * Math.PI / 180;
        int xFrom=xCenter, yFrom=yCenter;
        if (offset>0)
        {
            xFrom = (int) (xCenter + (offset * Math.cos(angle)));
            yFrom = (int) (yCenter + (offset * Math.sin(angle)));

        }
        int xTo = (int) (xCenter + (radius * Math.cos(angle)));
        int yTo = (int) (yCenter + (radius * Math.sin(angle)));
        gr.setColor(clr);

        gr.drawLine(xFrom, yFrom, xTo, yTo);
    }


    public void paintComponent(Graphics gr) {
        // чистим холст
        super.paintComponent(gr);

        // рисуем окружность часов
        gr.setColor(Color.RED);
        gr.drawOval(30, 30, 500, 500);

        // рисуем секундные отсечки
        for(int i = 0; i < 360/delta; ++i) {
            DrawMyLine(gr,Color.BLACK,i*delta,235,249);
        }

        // рисуем стрелки
        DrawMyLine(gr,Color.BLUE,angle_fast,0,230);
        DrawMyLine(gr,Color.GREEN,angle_slow,0,230);
    }
}
