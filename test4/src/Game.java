// Размеры игрового поля 40x30 (40 клеток высота, 30 клеток ширина) Размер одной клетки 20x20 пикселей
// Игровое поле нарисовать в виде сетки из линий, в игре используются семь деталей (см. рис)
// Детали появляются сверху в случайном порядке
// Управление стрелками клавиатуры: влево, вправо, вниз - падение детали, вверх – вращение детали.
// Точку вращения выбираем примерно по центру, вести подсчет очков.
// При окончании игры должна появляться соответствующая картинка или диалоговое окно, должны быть кнопки – Новая игра и Выход.

// При написании пригодился https://github.com/biblelamp/JavaExercises/blob/master/Games/GameTetris.java

import java.awt.*;
import java.awt.event.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

class Game extends JFrame {

    final String TITLE_OF_PROGRAM = "Тетрис";
    public static final int BLOCK_SIZE = 20; // размер блока
    public static final int FIELD_WIDTH = 30; // размерность колодца
    public static final int FIELD_HEIGHT = 40;
    final int START_LOCATION = 400;
    final int FIELD_DX = 17; // доп. раздвижка чтобы сетка не срезалась окном
    final int FIELD_DY = 38;
    public static final int LEFT = 37, UP = 38, RIGHT = 39,DOWN = 40;

    public static final int CELL_INT_COLOR = 0x00b050;
    public static final Color CELL_COLOR = new Color(CELL_INT_COLOR, false);
    public static final Color CELL_COLOR_MOVE = new Color(0xD7C325, false);


    final int TIMER_DELAY = 300; // шаг таймера, мс

    final int[] SCORES = {10, 30, 70, 150, 250};        // очки за число полных рядов
    public Timer timerBody;

    /* возможные фигуры (7 видов, описывающая окрестность с учетом поворотов 5x5)
       1x  2x x x  4x x    6x x    7x
        x   x   x   x       x x     x x
        x                   x x     x
        x   3 x    5x               x
        x   x x x   x x x
                        x                      */
    public static final int[][][] SHAPES = {
            {{0,0,1,0,0},{0,0,1,0,0},{0,0,1,0,0},{0,0,1,0,0},{0,0,1,0,0}},
            {{0,0,0,0,0},{0,1,1,1,0},{0,1,0,1,0},{0,0,0,0,0},{0,0,0,0,0}},
            {{0,0,0,0,0},{0,0,1,0,0},{0,1,1,1,0},{0,0,0,0,0},{0,0,0,0,0}},
            {{0,0,0,0,0},{0,1,1,0,0},{0,1,0,0,0},{0,0,0,0,0},{0,0,0,0,0}},
            {{0,0,0,0,0},{0,1,0,0,0},{0,1,1,1,0},{0,0,0,1,0},{0,0,0,0,0}},
            {{0,0,0,0,0},{0,1,1,0,0},{0,1,1,0,0},{0,1,1,0,0},{0,0,0,0,0}},
            {{0,1,0,0,0},{0,1,1,0,0},{0,1,0,0,0},{0,1,0,0,0},{0,0,0,0,0}}
    };

    int gameScore;
    int[][] mine = new int[FIELD_HEIGHT + 1][FIELD_WIDTH]; // колодец

    Canvas canvas = new Canvas();
    Figure figure = new Figure(mine);
    boolean gameOver = false;

    public static void main(String[] args) {
        new Game().gameStart();
    }

    Game() {
        setTitle(TITLE_OF_PROGRAM);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(START_LOCATION, START_LOCATION, FIELD_WIDTH * BLOCK_SIZE + FIELD_DX, FIELD_HEIGHT * BLOCK_SIZE + FIELD_DY);
        setResizable(false);
        canvas.setBackground(Color.DARK_GRAY);
        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (!gameOver) {
                    if (e.getKeyCode() == DOWN) figure.drop();
                    if (e.getKeyCode() == UP) figure.rotate();
                    if (e.getKeyCode() == LEFT || e.getKeyCode() == RIGHT) figure.move(e.getKeyCode());
                }
                canvas.repaint();
            }
        });
        add(BorderLayout.CENTER, canvas);

        setVisible(true);

        for (int col = 0; col < FIELD_WIDTH; col++)         // начальное заполнение земли
            mine[FIELD_HEIGHT][col] = 1;


        timerBody = new Timer(TIMER_DELAY,new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                canvas.repaint();
                checkFilling();
                if (figure.isTouchGround()) {
                    figure.leaveOnTheGround();
                    figure = new Figure(mine);
                    gameOver = figure.isCrossGround(); // если новая фигура коснулась земли
                } else
                    figure.stepDown();
            }
        });
    }

    void gameStart(){

        gameOver = false;
        gameScore = 0;
        for (int row = 0; row < FIELD_HEIGHT; row++)         // чистка колодца
            for (int col = 0; col < FIELD_WIDTH; col++)
            mine[row][col] = 0;
        timerBody.start();
    }

    void gameStop(){
        timerBody.stop();
        int n = JOptionPane.showOptionDialog(
                null,
                "Счет игры:  " + gameScore + ". Хотите улучшить результат?",
                "Игра закончена",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                new Object[]{"Новая игра", "Выход"},
                "Новая игра");

        if(n == JOptionPane.YES_OPTION)
        {
            gameStart();
        }
        else{
            System.exit(0);
        }
    }

    void checkFilling() { // проверка полного заполнения линий колодца

        int row = FIELD_HEIGHT - 1;
        int countFillRows = 0;
        while (row > 0) {
            int filled = 1;
            for (int col = 0; (col < FIELD_WIDTH) && (filled==1); col++)
                filled *= Integer.signum(mine[row][col]);
            if (filled > 0) {
                countFillRows++;
                for (int i = row; i > 0; i--) System.arraycopy(mine[i-1], 0, mine[i], 0, FIELD_WIDTH);
            } else
                row--;
        }
        if (countFillRows > 0) {
            gameScore += SCORES[countFillRows - 1];
            setTitle(TITLE_OF_PROGRAM + " : " + gameScore);
        }
    }

    class Canvas extends JPanel { // отрисовка холста
        @Override
        public void paint(Graphics g) {
            super.paint(g);

            int x=0, y=0;
            for (x = 0; x < FIELD_WIDTH; x++) {
                g.setColor(Color.GRAY);
                g.drawLine(x*BLOCK_SIZE, 0, x*BLOCK_SIZE, FIELD_HEIGHT*BLOCK_SIZE);

                for (y = 0; y < FIELD_HEIGHT; y++) {
                    g.setColor(Color.GRAY);
                    g.drawLine(0, y*BLOCK_SIZE, FIELD_WIDTH*BLOCK_SIZE, y*BLOCK_SIZE);
                    g.setColor(CELL_COLOR);
                    if (mine[y][x] > 0) {
                        g.fill3DRect(x * BLOCK_SIZE + 1, y * BLOCK_SIZE + 1, BLOCK_SIZE - 1, BLOCK_SIZE - 1, true);
                    }
                }
            }
            if (gameOver) {
                gameStop();
            } else
                figure.paint(g);
        }
    }
}