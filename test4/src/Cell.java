// ячейка (элемент для формирования фигур)

import java.awt.*;

class Cell {
    private int x, y;

    public Cell(int x, int y) {
        setX(x);
        setY(y);
    }

    void setX(int x) { this.x = x; }
    void setY(int y) { this.y = y; }

    int getX() { return x; }
    int getY() { return y; }

    void paint(Graphics g) {
        g.setColor(Game.CELL_COLOR_MOVE);
        g.fill3DRect(x*Game.BLOCK_SIZE+1, y*Game.BLOCK_SIZE+1, Game.BLOCK_SIZE-2, Game.BLOCK_SIZE-2, true);
    }
}