// фигуры

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

class Figure {
    final int FIGURE_SIZE = 5;
    private ArrayList<Cell> figure = new ArrayList<Cell>();
    private int[][] shape = new int[FIGURE_SIZE][FIGURE_SIZE];
    private int type;
    private int x = Game.FIELD_WIDTH/2-2, y = 0; // точка выпадения новой фигуры

    Random random = new Random();
    int[][] mine;

    Figure(int[][] mine) {
        type = random.nextInt(Game.SHAPES.length);  // выбираем случайную фигуру
        this.mine = mine;
//        if (size == 5) y = -1;
        for (int i = 0; i < FIGURE_SIZE; i++)
            System.arraycopy(Game.SHAPES[type][i], 0, shape[i], 0, Game.SHAPES[type][i].length);
        createFromShape();
    }

    void createFromShape() {
        for (int x = 0; x < FIGURE_SIZE; x++)
            for (int y = 0; y < FIGURE_SIZE; y++)
                if (shape[y][x] == 1) figure.add(new Cell(x + this.x, y + this.y));
    }

    boolean isTouchGround() {
        for (Cell cell : figure) if (mine[cell.getY() + 1][cell.getX()] > 0) return true;
        return false;
    }

    boolean isCrossGround() {
        for (Cell cell : figure) if (mine[cell.getY()][cell.getX()] > 0) return true;
        return false;
    }

    void leaveOnTheGround() {
        for (Cell cell : figure) mine[cell.getY()][cell.getX()] = Game.CELL_INT_COLOR;
    }

    boolean isTouchWall(int direction) {
        for (Cell cell : figure) {
            if (direction == Game.LEFT && (cell.getX() == 0 || mine[cell.getY()][cell.getX() - 1] > 0)) return true;
            if (direction == Game.RIGHT && (cell.getX() == Game.FIELD_WIDTH - 1 || mine[cell.getY()][cell.getX() + 1] > 0)) return true;
        }
        return false;
    }

    void move(int direction) {
        if (!isTouchWall(direction)) {
            int dx = direction - 38; // LEFT = -1, RIGHT = 1
            for (Cell cell : figure) cell.setX(cell.getX() + dx);
            x += dx;
        }
    }

    void stepDown() {
        for (Cell cell : figure) cell.setY(cell.getY() + 1);
        y++;
    }

    void drop() { while (!isTouchGround()) stepDown(); }

    boolean isWrongPosition() {
        for (int x = 0; x < FIGURE_SIZE; x++)
            for (int y = 0; y < FIGURE_SIZE; y++)
                if (shape[y][x] == 1) {
                    if (y + this.y < 0) return true;
                    if (x + this.x < 0 || x + this.x > Game.FIELD_WIDTH - 1) return true;
                    if (mine[y + this.y][x + this.x] > 0) return true;
                }
        return false;
    }

    void rotateShape(int direction) {
        for (int i = 0; i < FIGURE_SIZE/2; i++)
            for (int j = i; j < FIGURE_SIZE-1-i; j++)
                if (direction == Game.RIGHT) { // clockwise
                    int tmp = shape[FIGURE_SIZE-1-j][i];
                    shape[FIGURE_SIZE-1-j][i] = shape[FIGURE_SIZE-1-i][FIGURE_SIZE-1-j];
                    shape[FIGURE_SIZE-1-i][FIGURE_SIZE-1-j] = shape[j][FIGURE_SIZE-1-i];
                    shape[j][FIGURE_SIZE-1-i] = shape[i][j];
                    shape[i][j] = tmp;
                } else { // counterclockwise
                    int tmp = shape[i][j];
                    shape[i][j] = shape[j][FIGURE_SIZE-1-i];
                    shape[j][FIGURE_SIZE-1-i] = shape[FIGURE_SIZE-1-i][FIGURE_SIZE-1-j];
                    shape[FIGURE_SIZE-1-i][FIGURE_SIZE-1-j] = shape[FIGURE_SIZE-1-j][i];
                    shape[FIGURE_SIZE-1-j][i] = tmp;
                }
    }

    void rotate() {
        rotateShape(Game.RIGHT);
        if (!isWrongPosition()) {
            figure.clear();
            createFromShape();
        } else
            rotateShape(Game.LEFT);
    }

    void paint(Graphics g) {
        for (Cell cell : figure) cell.paint(g);
    }
}