//Дан двумерный массив целых чисел 24x24. Заполнить массив нулями и единицами,
//чтобы получилось шахматная доска. Размер одной клетки 4x4. Вывести массив
//в консольное окно, разделяя элементы массива пробелом

public class prog {

	public static void main(String[] args) {

		// создание
		int[][] mas = new int[24][24];

		// Переменная, определяющая значение элемента
		boolean cell = false;

		// заполнение
		for (int row=0; row<24; row++)
		{
			cell = row==0 ? false : row%4==0 ? !cell : cell;

			for (int col=0; col<24; col++)
			{
				// Если надо меняем значение элемента
				if (col%4==0)
					cell = !cell;
				// Заполняем текущий элемент массива
				mas[col][row] = cell ? 1 : 0;
			}
		}

		// вывод
		for (int row=0; row<24; row++) {
			for (int col=0; col<24; col++) {
				System.out.print(" " + mas[col][row]);
			}
			System.out.println();
		}
	}
}