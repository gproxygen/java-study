import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Color;

public class GuessingGame extends JFrame {
	private JTextField txtGuess;
	private JLabel lblOutput;
	private JLabel lblTriesToFinish;
	private int theNumber;
	private int numberOfTries;
	private int maxTries = 7;
	private JButton btnPlayAgain;
	private JButton btnGuess;

	public void endGame() {
		lblTriesToFinish.setText("");
		btnPlayAgain.setVisible(true);
		btnGuess.setVisible(false);		
	}

	public void checkGuess() {
		String guessText = txtGuess.getText();
		String message = "";
		try {
			int guess = Integer.parseInt(guessText);
			numberOfTries++;
			if (numberOfTries == maxTries) {
				message = "�� ���������!";
				endGame();
			} else {
				if (guess < theNumber)
					message = guess + " ������� ���������";
				else if (guess > theNumber)
					message = guess + " ������� �������";
				else {
					message = guess + " ��������� �����, �� ����������!";
					endGame();
				}
			}

		} catch (Exception e) {
			message = "������� ����� �� 1 �� 100";
		} finally {
			lblOutput.setText(message);
			lblTriesToFinish.setText("�������� �������: " + (maxTries - numberOfTries));
			txtGuess.requestFocus();
			txtGuess.selectAll();
		}
	}

	public GuessingGame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("������ �����");
		getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("����� ���������� � ���� ������ ����");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 35, 414, 20);
		getContentPane().add(lblNewLabel);

		lblOutput = new JLabel("������� ����� � ������� �������");
		lblOutput.setHorizontalAlignment(SwingConstants.CENTER);
		lblOutput.setBounds(10, 189, 414, 20);
		getContentPane().add(lblOutput);

		btnGuess = new JButton("�������");
		btnGuess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkGuess();
			}
		});
		btnGuess.setBounds(151, 126, 131, 33);
		getContentPane().add(btnGuess);

		JLabel lblGuessNumberBetween = new JLabel("�������� ����� �� 1 �� 100");
		lblGuessNumberBetween.setBounds(89, 90, 173, 20);
		getContentPane().add(lblGuessNumberBetween);

		txtGuess = new JTextField();
		txtGuess.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtGuess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkGuess();
			}
		});
		txtGuess.setBounds(272, 90, 47, 20);
		getContentPane().add(txtGuess);
		txtGuess.setColumns(10);

		lblTriesToFinish = new JLabel("�������� �������: " + maxTries);
		lblTriesToFinish.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblTriesToFinish.setForeground(Color.RED);
		lblTriesToFinish.setHorizontalAlignment(SwingConstants.CENTER);
		lblTriesToFinish.setBounds(10, 166, 414, 20);
		getContentPane().add(lblTriesToFinish);
		
				btnPlayAgain = new JButton("����� ����");
				
						btnPlayAgain.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								newGame();
							}
						});
						btnPlayAgain.setFont(new Font("Tahoma", Font.PLAIN, 14));
						btnPlayAgain.setBounds(151, 217, 131, 33);
						getContentPane().add(btnPlayAgain);
	}

	public void newGame() {
		btnPlayAgain.setVisible(false);
		btnGuess.setVisible(true);
		theNumber = (int) (Math.random() * 100 + 1);
		numberOfTries = 0;
		lblOutput.setText("������� ����� � ������� �������");
		lblTriesToFinish.setText("�������� �������: " + maxTries);
	}

	public static void main(String[] args) {
		GuessingGame theGame = new GuessingGame();
		theGame.newGame();
		theGame.setSize(new Dimension(450, 300));
		theGame.setVisible(true);
	}
}
