// создание нового сообщения
$(document).on('click', '#menu-message', function () {

    var html = `
    <h4>Новое сообщение</h4>
    <form id='message_form'>
        <div class="form-group">
            <label for="message">Введите исходный текст</label>
            <textarea type="text" rows="8" class="form-control" name="message" id="message" required />
        </div>    
        <button type='submit' class='btn btn-primary'>Сохранить</button>
    </form>
`;

    $('#response').html('');
    $('#content').html(html);
});

// выполнение кода при отправке формы 
$(document).on('submit', '#message_form', function () {  
    // получаем данные формы 
    var message_form = $(this);
    var form_data = JSON.stringify(message_form.serializeObject());
    
    // отправить данные формы в API 
    $.ajax({
        url: "api/actions/create_message.php",
        type: "POST",
        contentType: 'application/json',
        data: form_data,
        success: function (result) {            
            // в случае удачного завершения запроса к серверу            
            v = JSON.parse(form_data);
            $('#response').html(
                "<div class='alert alert-success'>Ваше сообщение: <br><b>"+v['message']+"</b><br>успешно сохранено в базе данных</div>"
                );
            message_form.find('#message').val('');
        },
        error: function (xhr, resp, text) {
            // сохранение не удалось
            $('#response').html(
                "<div class='alert alert-danger'>Невозможно сохранить Ваше сообщение</div>"
                );
        }
    });
    return false;
});

