// домашняя страница с сообщениями

$(document).on('click', '#menu-home', function () {
    
    var html = '';
    $('#response').html('Производится загрузка сообщений, пожалуйста подождите');

    // читаем сообщения 
    $.ajax({
        url: "api/actions/get_messages.php",
        
        success: function (result) { 
            data = JSON.parse(result);
            html = createTableFromJSON(data);
            $('#response').html('Сообщения успешно загружены');            
            $('#content').html(html);
        },
        error: function (xhr, resp, text) {            
            $('#response').html(
                "<div class='alert alert-danger'>Невозможно загрузить сообщения</div>"
                );
        }
    });
});