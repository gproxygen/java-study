// функция для преобразования значений формы в формат JSON 
$.fn.serializeObject = function(){

    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function createTableFromJSON(v){

    col = [];
    for (i = 0; i < v.length; i++) {
            for (key in v[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
    }

    var tb = '<table class="table">';
    for (i = 0; i < v.length; i++) {
            tb+='<tr>';
            for (j = 0; j < col.length; j++) {
                tb+='<td>'+v[i][col[j]]+'</td>';
            }
            tb+='</tr>'
    }
    return tb;
}




    