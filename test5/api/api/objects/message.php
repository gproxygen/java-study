<?php

class Message {
 
    private $conn;
    private $table_name = "messages";
 
    // свойства объекта 
    public $id_user;
    public $message;
    public $dt;
 
    // конструктор класса 
    public function __construct($db) {
        $this->conn = $db;
    }

    // добавление записи
    function create() {
    
        // Вставляем запрос 
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    id_user = :id_user,
                    message = :message,
                    dt = :dt";
    
        // подготовка запроса 
        $stmt = $this->conn->prepare($query);
    
        // инъекция 
        $this->id_user=htmlspecialchars(strip_tags($this->id_user));
        $this->message=htmlspecialchars(strip_tags($this->message));
    
        // привязываем значения 
        $stmt->bindParam(':id_user', $this->id_user);
        $stmt->bindParam(':message', $this->message);
        $stmt->bindParam(':dt', $this->dt);
    
        // Если выполнение успешно, то информация сохранена в БД
        if($stmt->execute()) {
            return true;
        }    
        return false;
    }

    function get() {
        $s = 'select FROM_UNIXTIME(dt) as grirorian_date, login, message from messages INNER JOIN users ON users.id = messages.id_user';
        $query = $this->conn->query($s);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}