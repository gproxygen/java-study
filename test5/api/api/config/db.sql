CREATE DATABASE IF NOT EXISTS `test5`;
USE `test5`;


CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pass` varchar(50) NOT NULL,
  `login` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

REPLACE INTO `users` (`id`, `pass`, `login`) VALUES
	(1, '1234567890', 'Петя'),
	(2, '0987654321', 'Лена');


CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `dt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

REPLACE INTO `messages` (`id`, `id_user`, `message`, `dt`) VALUES
	(1, 1, 'Первое сообщение от пользователя 1', 1572898449),
	(2, 1, 'Кто-нибудь видит мои сообщения?', 1572900662),
	(3, 2, 'Да, все в порядке', 1572904803);