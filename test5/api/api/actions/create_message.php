<?php

include_once '../config/connect.php';
include_once '../objects/message.php';

// подключаем БД
$database = new Database();
$db = $database->getConnection();
 
// создание объекта 'Message'
$message = new Message($db);

// получаем данные 
$data = json_decode(file_get_contents("php://input"));
 
// устанавливаем значения
$message->id_user = $_SESSION['user_id'];        // отправитель
$message->message = $data->message;             // текст сообщения

$date = new DateTime();
$message->dt = $date->getTimestamp();      // время (unixtime)
// создание пользователя 
if (
    !empty($message->id_user) &&
    !empty($message->message) &&
    !empty($message->dt) &&
    $message->create()
) {
    // устанавливаем код успешного ответа 
    http_response_code(200);    
    echo json_encode(array("message" => "Сообщение успешно добавлено"));
}
 
// сообщение, если не удаётся сохранить сообщение
else {
    // устанавливаем код ответа 
    http_response_code(400);     
    echo json_encode(array("message" => "Неверный запрос или отсутствует доступ"));
}
?>