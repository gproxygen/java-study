<?php

include_once '../config/connect.php';
include_once '../objects/message.php';

// подключаем БД
$database = new Database();
$db = $database->getConnection();
 
// создание объекта 'Message'
$message = new Message($db);
$data = json_encode($message->get());

echo($data);

?>